from PyQt5.QtCore import *
import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot
import logging
from ui import *

logger = logging.getLogger(__name__)
logging.basicConfig(encoding='utf-8', level=logging.DEBUG)

APP_NAME = 'TopPlacemaker'
        
BAKGROUND_COLOR_1 = QColor('#111111')
BAKGROUND_COLOR_2 = QColor('#4f4f4f')
TEXT_COLOR = QColor('#111111')

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.settings = QSettings(APP_NAME,'pshir', self)

        self.resize(1200, 800)
        self.setWindowIcon(QIcon(f'{APP_NAME}.png'))
        self.setWindowTitle(APP_NAME)
        
        # main container
        self.container = QFrame()
        # TODO: fix style
        self.container.setObjectName('main-continer')
        self.container.setStyleSheet('#main-continer { background-color: #4f4f4f }')
        self.layout = QHBoxLayout()
        self.container.setLayout(self.layout)
        self.setCentralWidget(self.container)

        self.menubar = QMenuBar()
        # TODO: fix style
        self.menubar.setObjectName('menubar')
        self.menubar.setStyleSheet('#menubar { background-color: #4f4f4f }')
        self.setMenuBar(self.menubar)
        actionFile = self.menubar.addMenu("File")
        # TODO: fix style
        actionFile.setObjectName('f-menubar')
        actionFile.setStyleSheet('#f-menubar { background-color: #4f4f4f }')
        actionFile.addAction("New")
        actionFile.addAction("Open")
        actionFile.addAction("Save")
        actionFile.addSeparator()
        actionFile.addAction("Quit")
        self.menubar.addMenu("Edit")
        self.menubar.addMenu("View")
        self.menubar.addMenu("Help")

        self.gr_scene = GScene(self.container)
        self.gr_view = GView(self.gr_scene)

        # TODO: table controller (+cols order)
        data = {'N':['1','2','3','4'],
                'Type':['1','2','1','3'],
                'Source':['1','2','1','3'],
                'Gate':['1','2','1','3'],
                'Drain':['1','2','1','3']}
        self.tr_table = GTableView(data, 4,5)
        # TODO: fix style

        # TODO: user can change container size
        ## container -> container_side
        self.container_side = QFrame(self.container)
        self.container_side.setFrameShape(QFrame.StyledPanel)
        self.container_side.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred))
        self.container_side.layout = QVBoxLayout()
        self.container_side.setLayout(self.container_side.layout)

        ### container -> container_side -> buttons
        self.container_side.layout.addWidget(self.tr_table, alignment=Qt.AlignHCenter)
        # self.container_side.layout.addWidget(self.buttons, alignment=Qt.AlignHCenter)

        self.layout.addWidget(self.gr_view)
        self.layout.addWidget(self.container_side)
        # self.layout.addWidget(self.tr_table)

        self.load_settings()
        # self.gr_scene.showGrid = False
        self.show()
    

    def load_settings(self):
        self.setGeometry(self.settings.value('geo', QRect(1200, 800, 1000, 300)))

    def save_settings(self):
        self.settings.setValue('geo', self.geometry())
        
    def closeEvent(self, event):
        self.save_settings()
        event.accept()
        #  event.ignore()
        #  self.hide()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())

