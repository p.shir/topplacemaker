     
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem

class GTableView(QTableWidget):

    # {{ background-color: {bg_color}; }}
    _styleSheet =  """
    QWidget{
        background-color: #444444 ;
    }
    QHeaderView::section {
    background-color: #646464;
    padding: 4px;
    font-size: 14pt;
    border-style: none;
    border-bottom: 1px solid #fffff8;
    border-right: 1px solid #fffff8;
}

QHeaderView::section:horizontal
{
    border-top: 1px solid #fffff8;
}

QHeaderView::section:vertical
{
    border-left: 1px solid #fffff8;
}
    """

    def __init__(self, data, *args):
        QTableWidget.__init__(self, *args)
        self.data = data
        self.setData()
        self.resizeColumnsToContents()
        self.resizeRowsToContents()

        # style = self._styleSheet.format(bg_color='#111111')
        self.setStyleSheet(self._styleSheet)
        # self.setStyleSheet(style)
 
    def setData(self): 
        horHeaders = []
        for n, key in enumerate((self.data.keys())):
            horHeaders.append(key)
            for m, item in enumerate(self.data[key]):
                newitem = QTableWidgetItem(item)
                self.setItem(m, n, newitem)
        self.setHorizontalHeaderLabels(horHeaders)