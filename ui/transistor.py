from PyQt5 import QtGui
from PyQt5.QtCore import *
from typing import Optional
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class GTransistor(QGraphicsItem):
    """Transistor ui representation"""
    
    def __init__(self, parent: Optional[QWidget], id: int = 0, ttype: bool = False, size: int = 200) -> None:
        super().__init__()

        self.ttype = ttype
        self.name = f't{"p" if ttype else "n"}{int(id)}'
        self.size = 200
        self.s_index = 1
        self.d_index = 2
        self.g_index = 3
        self._show_text = True
        self.hovered = False
        self.movable = True
        
        self.initAssets()
        self.initUI()
        
    def initUI(self):
        """Set up this ``QGraphicsItem``"""
        self.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setAcceptHoverEvents(True)

    def initAssets(self):
        """Initialize ``QObjects`` like ``QColor``, ``QPen`` and ``QBrush``"""
        self._text_font = QFont("Ubuntu", 12)
        self._name_font = QFont("Ubuntu", 40)
        self._name_font.setBold(True)
        self._text_color = QColor("#aaaaaa")


        self._pen_name = QPen(QColor('#777777'))
        self._pen_name_hover = QPen(QColor('#ff0000'))

        if self.ttype:
            self._sd_brush = QBrush(QColor("#8888e2"))  # source drain positive
        else:
            self._sd_brush = QBrush(QColor("#e28888"))  # source drain negative
        self._g_brush = QBrush(QColor("#88e288"))  # gate
        self._background_brush = QBrush(QColor("#9b9b9b"))
        self._text_brush = QBrush(QColor("#111111"))
        self._name_brush = QBrush(QColor("#666666"))

    def boundingRect(self) -> QRectF:
        """Defining Qt' bounding rectangle"""
        return QRectF(0, 0, self.size, self.size).normalized()

    def hoverEnterEvent(self, event: QGraphicsSceneHoverEvent) -> None:
        """Handle hover effect"""
        self.hovered = True
        self.update()

    def hoverLeaveEvent(self, event: QGraphicsSceneHoverEvent) -> None:
        """Handle hover effect"""
        self.hovered = False
        self.update()


    def paint(self, painter: QPainter, QStyleOptionGraphicsItem, widget=None):
        """Painting the transistor"""
        padding = round(self.size * 0.05)
        x_gate = round(self.size / 3)
        w_gate = x_gate  # round(self.size / 3)

        # TODO: get info from Transistor class
        common_area_l = False
        common_area_r = False
        common_area_gate_top = True
        common_area_gate_bot = False
        x_sd = 0 if common_area_l else padding
        w_sd = self.size - padding*2 + (padding - x_sd) + padding*bool(common_area_r)
        y_g = 0 if common_area_gate_top else padding
        h_g = self.size - padding*2 + (padding - y_g) + padding*bool(common_area_gate_bot)

        painter.setBrush(self._background_brush)
        painter.drawRect(0,0,self.size, self.size)
        painter.setBrush(self._sd_brush)
        painter.drawRect(x_sd, padding, w_sd, self.size - 2*padding)
        # painter.drawRect(padding, padding, self.size - 2*padding, self.size - 2*padding)
        painter.setBrush(self._g_brush)
        painter.drawRect(x_gate, y_g, w_gate, h_g)
        # painter.drawRect(x_gate, padding, w_gate, self.size - 2* padding)

        # TODO: add setter
        if self._show_text:
            x_text_s = round(self.size *0.15)
            x_text_g = round(self.size *0.15 + x_gate)
            x_text_d = round(self.size *0.15 + x_gate*2)
            y_text_top = round(self.size *0.2)
            y_text_bottom = round(self.size *0.85)
            painter.setBrush(self._text_brush)
            painter.setFont(self._text_font)
            painter.drawText(x_text_s, y_text_top, str(self.s_index))
            painter.drawText(x_text_s, y_text_bottom, str(self.s_index))
            painter.drawText(x_text_g, y_text_top, str(self.g_index))
            painter.drawText(x_text_g, y_text_bottom, str(self.g_index))
            painter.drawText(x_text_d, y_text_top, str(self.d_index))
            painter.drawText(x_text_d, y_text_bottom, str(self.d_index))

            # TODO: transparent transistor name
            painter.setBrush(self._name_brush)
            painter.setFont(self._name_font)
            if self.hovered:
                painter.setPen(self._pen_name_hover)
            else:
                painter.setPen(self._pen_name)
            rect = QRect(round(self.size/5), round(self.size/5), round(self.size/5*3), round(self.size/5*3))
            painter.drawText(rect, Qt.AlignCenter, self.name)

    def mouseReleaseEvent(self, event):
        """Overriden event to handle when we moved, selected or deselected this `Node`"""
        super().mouseReleaseEvent(event)

        # handle when grNode moved
        if self._was_moved:
            self._was_moved = False
            p = self.pos()
            x = p.x()
            y = p.y()
            new_x = round(x / self.size)*self.size
            new_y = round(y / self.size)*self.size
            self.setPos(QPointF(new_x, new_y))
            print(p)
            # self.node.scene.history.storeHistory("Node moved", setModified=True)

            # self.node.scene.resetLastSelectedStates()
            # self.doSelect()     # also trigger itemSelected when node was moved

            # we need to store the last selected state, because moving does also select the nodes
            # self.node.scene._last_selected_items = self.node.scene.getSelectedItems()

            # now we want to skip storing selection
            # return

        # handle when grNode was clicked on
        # if self._last_selected_state != self.isSelected() or self.node.scene._last_selected_items != self.node.scene.getSelectedItems():
            # self.node.scene.resetLastSelectedStates()
            # self._last_selected_state = self.isSelected()
            # self.onSelected()

    def mouseMoveEvent(self, event):
        """Overridden event to detect that we moved with this `Node`"""
        super().mouseMoveEvent(event)

        # optimize me! just update the selected nodes
        # for node in self.scene().scene.nodes:
            # if node.grNode.isSelected():
                # node.updateConnectedEdges()
        self._was_moved = True

    def mouseDoubleClickEvent(self, event):
        """Overriden event for doubleclick. Resend to `Node::onDoubleClicked`"""
        self.movable = not(self.movable)
        self.setFlag(QGraphicsItem.ItemIsMovable, enabled=self.movable)
