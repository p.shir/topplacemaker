- [ ] ui
    - [x] transistor name text
    - [x] magnet transistor
    - [ ] pin transistor (double click)
    - [ ] pin transistor (menu on transisotor)
    - [ ] scene zoom
    - [ ] menubar + shortcuts
    - [ ] fast menu bottom left
- [ ] simple transistor item
- [ ] user stories algos
- [ ] table style
- [ ] load netlist data (and represent in table)
- [ ] entites (UML diagrams)
- [ ] simple place algo
- [ ] work virtual grid area (basend on transistor amount)
- [ ] user change work virtual grid area 
- [ ] add logger



menu elements
- fi


user can:

v1:
- load netlist file
- choice grid size
- run find placement
- choice 1 from several variants

v2:
- run tracer

v3:
- choice placer algo

v4:
- place transistors manual

v5:
- save and load placer results (generic file `json`???)
- save and load placer results (cpp TopDesign file) (generic -> cpp translator)

v6:
- 